<?php



Route::group(['prefix' => 'Admin', 'middleware' => 'is_admin_middleware'], function () {
    Route::get('/',function (){ return view('Admin.home'); })->name('admin_home');

    ////begin rating
    Route::group(['prefix' => 'Rating', 'middleware' => 'is_admin_middleware'], function () {
        Route::get("/load", 'Admin\RatingController@load')->name('user_rating');
        Route::post("/user_rating", 'Admin\RatingController@post_user_rating')->name('post_user_rating');
    });

    ////begin option
    Route::group(['prefix' => 'Option', 'middleware' => 'is_admin_middleware'], function () {
        Route::get("/load", 'Admin\OptionController@load')->name('option');
        Route::post("/option", 'Admin\OptionController@post_option')->name('post_option');
    });

    /// begin category
    Route::group(['prefix' => 'ProductCategory', 'middleware' => 'is_admin_middleware'], function () {
        Route::get("/load", 'Admin\ProductCategoryController@load')->name('product_category');
        Route::post("/product_category", 'Admin\ProductCategoryController@post_product_category')->name('post_product_category');
    });

    ///begin product type and style
    Route::group(['prefix' => 'ProductType', 'middleware' => 'is_admin_middleware'], function () {
        Route::get("/load", 'Admin\ProductTypeController@load')->name('product_type');
        Route::post("/product_type", 'Admin\ProductTypeController@post_product_type')->name('post_product_type');
    });
    Route::group(['prefix' => 'ProductStyle', 'middleware' => 'is_admin_middleware'], function () {
        Route::get("/load", 'Admin\ProductStyleController@load')->name('product_style');
        Route::post("/product_style", 'Admin\ProductStyleController@post_product_style')->name('post_product_style');
    });



    Route::group(['prefix' => 'Permission', 'middleware' => 'is_admin_middleware'], function () {
        Route::get("/load", 'Admin\PermissionController@load')->name('load_permission');
        Route::get("/insert", 'Admin\PermissionController@insert')->name('insert_permission');
        Route::post("/post_add_permission", 'Admin\PermissionController@post_add_permission')->name('post_add_permission');
        Route::group(['prefix' => 'Ajax'], function () {
            Route::get('/delete/{id}','Admin\PermissionController@delete')->name('delete_permission');
        });
    });
    Route::group(['prefix' => 'Role', 'middleware' => 'is_admin_middleware'], function () {
        Route::get("/load", 'Admin\RoleController@load')->name('load_role');
        Route::get("/insert", 'Admin\RoleController@insert')->name('insert_role');
        Route::get("/edit/{id}", 'Admin\RoleController@edit')->name('edit_role');
        Route::post("/post_add_role", 'Admin\RoleController@post_add_role')->name('post_add_role');
        Route::post("/post_edit_role", 'Admin\RoleController@post_edit_role')->name('post_edit_role');
        Route::group(['prefix' => 'Ajax'], function () {
            Route::get('/delete/{id}','Admin\RoleController@delete')->name('delete_role');
        });
    });
    Route::group(['prefix' => 'User', 'middleware' => 'is_admin_middleware'], function () {
        Route::get("/load", 'Admin\UserController@load')->name('load_user');
        Route::get("/insert", 'Admin\UserController@insert')->name('insert_user');
        Route::get("/edit/{id}", 'Admin\UserController@edit')->name('edit_user');
        Route::group(['prefix' => 'Ajax'], function () {
            Route::get('/delete/{id}','Admin\UserController@delete')->name('delete_record');
            Route::post('/edit_user_role','Admin\UserController@edit_user_role')->name('edit_user_role_record');
            Route::post('/edit_user_username','Admin\UserController@edit_user_username')->name('edit_user_username');
            Route::post('/edit_user_email','Admin\UserController@edit_user_email')->name('edit_user_email');
            Route::get('/edit_user_role','Admin\UserController@edit_user_role')->name('edit_user_role_record');
            Route::post('/permission_for_role','Admin\UserController@permission_for_role')->name('permission_for_role');
        });
    });


    ////begin model name
    Route::group(['prefix' => 'ModelName', 'middleware' => 'is_admin_middleware'], function () {
        Route::get("/load/{slug}", 'Admin\ModelNameController@load')->name('load_model_name');
        Route::get("/insert/{slug}", 'Admin\ModelNameController@insert')->name('insert_model_name');
        Route::get("/edit/{slug}/{id}", 'Admin\ModelNameController@edit')->name('edit_model_name');
        Route::post("/post_insert/{slug}", 'Admin\ModelNameController@post_insert')->name('post_insert');
        Route::post("/post_edit_model_name/{slug}", 'Admin\ModelNameController@post_edit_model_name')->name('post_edit_model_name');
        Route::group(['prefix' => 'Ajax'], function () {
            Route::get('/delete/{id}','Admin\ProductStyleController@delete')->name('delete_model_name');
        });
    });
    Route::get("/home", "Admin\DashboardController@index")->name('Dashboard_Home_Admin')->middleware('Check_Admin_Login');

});
?>