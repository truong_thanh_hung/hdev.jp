<?php
/*
|--------------------------------------------------------------------------
| front Routes
|--------------------------------------------------------------------------
| tại đây ta đăng kí các routing dành cho client người dùng
|
*/
Route::get('/404',function (){
    return view('errors.404');
})->name('404');

Route::group(['middleware' => 'locale'], function() {

    Route::get('change-language/{language}', 'Controller@changeLanguage')->name('change-language');
    Route::get('/login', 'LoginController@get_login')->name('get_login');///vào đây vừa login
    Route::post("/login", 'LoginController@post_login')->name('post_login'); /// đây là nơi post login để check xem có login hay không
    Route::get("/logout", 'LoginController@logout')->name('logout'); /// logout hệ thống

    Route::get("/register", 'LoginController@get_register')->name('get_register'); /// đăng kí tài khoản
    Route::post("/register", 'LoginController@post_register')->name('post_register'); /// đăng kí tài khoản
    ///
    ///
    ///
    ///
    ///
    ///
    ///
    ///
    Route::get('/', 'homeController@index')->name('home');

    Route::get("/{slug}/san-pham", 'homeController@list_product')->name('list_product_model_name');
    Route::get("/{slug}/shop", 'homeController@list_product')->name('shop');
    Route::get("/{slug}/product-sale", 'homeController@list_product')->name('product-sale');
    Route::get("/{slug}/product-sale2", 'homeController@list_product')->name('product-sale2');
    Route::group(['prefix' => 'Ajax'], function () {
        Route::get('/{slug}/load/product','homeController@load_product')->name('ajax_load_product');
    });

    Route::get("/{slug}/san-pham", 'homeController@list_product')->name('list_product_model_name');
    Route::get("/{slug}/gio-hang", 'homeController@cart')->name('cart_model_name');
    Route::get("/{slug}/tin-tuc", 'homeController@blog')->name('blog_model_name');
    Route::get("/{slug}/chung-toi", 'homeController@about')->name('about_model_name');
    Route::get("/{slug}/lien-he", 'homeController@contact')->name('contact_model_name');
    Route::get("/{slug}/san-pham/{id}", 'homeController@product_detail')->name('product_detail_model_name');

});

?>