<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        ///user supper admin
        DB::table('users')->insert(
            [
                'name'       => 'Trương Thanh Hùng',
                'email'      => 'thanhhung.code@gmail.com',
                'avatar'     => 'upload/images/avatar.jpg',
                'password'   => bcrypt('123456789'),
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ],
            [
                'name'       => 'Quản trị Quần Áo',
                'email'      => 'quanao.code@gmail.com',
                'avatar'     => 'upload/images/avatar1.jpg',
                'password'   => bcrypt('123456789'),
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ],
            [
                'name'       => 'Quản trị đồng hồ',
                'email'      => 'dongho.code@gmail.com',
                'avatar'     => 'upload/images/avatar2.jpg',
                'password'   => bcrypt('123456789'),
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ]
        );
        ///category
        // id	name	slug	excerpt	thumbnail	background	description	image_seo	
        // keyword_seo	description_seo	created_at	updated_at
        DB::table('product_category')->insert([
            [
                'name'            => 'quần áo',
                'slug'            => 'quan-ao',
                'excerpt'         => 'excerpt excerpt excerpt',
                'thumbnail'       => null,
                'background'      => null,
                'description'     => 'đây là thời trang thuộc shop thời trang.vn là loại thời trang luôn uy tín chất lượng hàng đầu. liên hệ với chúng tôi để đực đặt hàng ttos',
                'image_seo'       => null,
                'keyword_seo'     => 'thời trang, thời trang xuất khẩu, thời trang ngoại, thời trang chất lượng, thời trang giá rẻ, thời trang xách tay',
                'description_seo' => 'đây là thời trang thuộc shop thời trang.vn là loại thời trang luôn uy tín chất lượng hàng đầu. liên hệ với chúng tôi để đực đặt hàng ttos',
                'created_at'      => date('Y-m-d H:i:s'),
                'updated_at'      => date('Y-m-d H:i:s')
            ],[
                'name'            => 'đồng hồ',
                'slug'            => 'dong-ho',
                'excerpt'         => 'excerpt excerpt excerpt đồng hồ',
                'thumbnail'       => null,
                'background'      => null,
                'description'     => 'đồng hồ, đồng hồ xuất khẩu, đồng hồ ngoại, đồng hồ chất lượng, đồng hồ giá rẻ, đồng hồ xách tay',
                'image_seo'       => null,
                'keyword_seo'     => 'thời trang, thời trang xuất khẩu, thời trang ngoại, thời trang chất lượng, thời trang giá rẻ, thời trang xách tay',
                'description_seo' => 'đây là đồng hồ thuộc shop dongho.vn là loại đồng hồ luôn uy tín chất lượng hàng đầu. liên hệ với chúng tôi để đực đặt hàng ttos',
                'created_at'      => date('Y-m-d H:i:s'),
                'updated_at'      => date('Y-m-d H:i:s')
            ],
        ]);
        // name	slug excerpt thumbnail	background	description	product_category_id	image_seo	
        // keyword_seo	description_seo	created_at	updated_at
        DB::table('product_type')->insert(
            [
                [
                    'name'                => 'áo thun nam',
                    'slug'                => 'ao-thun-nam',
                    'excerpt'             => 'excerpt excerpt excerpt áo thun nam',
                    'thumbnail'           => null,
                    'background'          => null,
                    'description'         => 'áo thun nam, áo thun nam xuất khẩu, áo thun nam ngoại, áo thun nam chất lượng, áo thun nam giá rẻ, đồng hồ xách tay',
                    'product_category_id' => 1,
                    'image_seo'           => null,
                    'keyword_seo'         => 'áo thun nam đẹp, áo thun nam giá shock, áo thun nam xách tay',
                    'description_seo'     => 'đây là áo thun nam  thuộc shop dongho.vn là loại đồng hồ luôn uy tín chất lượng hàng đầu. liên hệ với chúng tôi để đực đặt hàng ttos',
                    'created_at'          => date('Y-m-d H:i:s'),
                    'updated_at'          => date('Y-m-d H:i:s')
                ],
                [
                    'name'                => 'áo sơ mi nữ',
                    'slug'                => 'ao-so-mi-nu',
                    'excerpt'             => 'excerpt excerpt excerpt áo sơ mi nữ',
                    'thumbnail'           => null,
                    'background'          => null,
                    'description'         => 'áo sơ mi nữ, áo sơ mi nữ xuất khẩu, áo sơ mi nữ ngoại, áo sơ mi nữ chất lượng, áo sơ mi nữ giá rẻ, đồng hồ xách tay',
                    'product_category_id' => 1,
                    'image_seo'           => null,
                    'keyword_seo'         => 'áo sơ mi nữ đẹp, áo sơ mi nữ giá shock, áo sơ mi nữ xách tay',
                    'description_seo'     => 'đây là áo sơ mi nữ  thuộc shop dongho.vn là loại áo sơ mi nữ luôn uy tín chất lượng hàng đầu. liên hệ với chúng tôi để đực đặt hàng ttos',
                    'created_at'          => date('Y-m-d H:i:s'),
                    'updated_at'          => date('Y-m-d H:i:s')
                ],
                [
                    'name'                => 'đồng hồ nam',
                    'slug'                => 'dong-ho-nam',
                    'excerpt'             => 'excerpt excerpt excerpt đồng hồ nam',
                    'thumbnail'           => null,
                    'background'          => null,
                    'description'         => 'đồng hồ nam, đồng hồ nam xuất khẩu, đồng hồ nam ngoại, đồng hồ nam chất lượng, đồng hồ nam giá rẻ, đồng hồ xách tay',
                    'product_category_id' => 2,
                    'image_seo'           => null,
                    'keyword_seo'         => 'đồng hồ nam đẹp, đồng hồ nam giá shock, đồng hồ nam xách tay',
                    'description_seo'     => 'đây là đồng hồ nam  thuộc shop dongho.vn là loại đồng hồ luôn uy tín chất lượng hàng đầu. liên hệ với chúng tôi để đực đặt hàng ttos',
                    'created_at'          => date('Y-m-d H:i:s'),
                    'updated_at'          => date('Y-m-d H:i:s')
                ],
                [
                    'name'                => 'đồng hồ nữ',
                    'slug'                => 'dong-ho-nu',
                    'excerpt'             => 'excerpt excerpt excerpt đồng hồ nữ',
                    'thumbnail'           => null,
                    'background'          => null,
                    'description'         => 'đồng hồ nữ, đồng hồ nữ xuất khẩu, đồng hồ nữ ngoại, đồng hồ nam chất lượng, đồng hồ nam giá rẻ, đồng hồ xách tay',
                    'product_category_id' => 2,
                    'image_seo'           => null,
                    'keyword_seo'         => 'đồng hồ nữ đẹp, đồng hồ nữ giá shock, đồng hồ nữ xách tay',
                    'description_seo'     => 'đây là đồng hồ nữ  thuộc shop dongho.vn là loại đồng hồ luôn uy tín chất lượng hàng đầu. liên hệ với chúng tôi để đực đặt hàng ttos',
                    'created_at'          => date('Y-m-d H:i:s'),
                    'updated_at'          => date('Y-m-d H:i:s')
                ],
                [
                    'name'                => 'đồng hồ cặp',
                    'slug'                => 'dong-ho-cap',
                    'excerpt'             => 'excerpt excerpt excerpt đồng hồ cặp',
                    'thumbnail'           => null,
                    'background'          => null,
                    'description'         => 'đồng hồ cặp, đồng hồ cặp xuất khẩu, đồng hồ nữ ngoại, đồng hồ nam chất lượng, đồng hồ nam giá rẻ, đồng hồ xách tay',
                    'product_category_id' => 2,
                    'image_seo'           => null,
                    'keyword_seo'         => 'đồng hồ cặp đẹp, đồng hồ cặp giá shock, đồng hồ nữ xách tay',
                    'description_seo'     => 'đây là đồng hồ cặp  thuộc shop dongho.vn là loại đồng hồ luôn uy tín chất lượng hàng đầu. liên hệ với chúng tôi để đực đặt hàng ttos',
                    'created_at'          => date('Y-m-d H:i:s'),
                    'updated_at'          => date('Y-m-d H:i:s')
                ],
            ]
        );
        // name	slug	price	price_sale	price_unit	description	
        // count	count_unit	thumbnail	post_by_user_id	product_type_id	like	view	
        // publish	image_seo	keyword_seo	description_seo	created_at	updated_at	
        DB::table('product')->insert(
            [
                [
                    'name'            => 'áo thun nam adidas có cổ',
                    'slug'            => 'ao-thun-nam-adidas-co-co',
                    'price'           => '3000',
                    'price_sale'      => '1000',
                    'price_unit'      => '¥',
                    'description'     => 'áo thun nam adidas có cổ,áo thun nam adidas có cổ xuất khẩu, áo thun nam ngoại, áo thun nam chất lượng, áo thun nam giá rẻ, đồng hồ xách tay',
                    'count'           => '-1',
                    'count_unit'      => 'cái',
                    'thumbnail'       => null,
                    'post_by_user_id' => 1,
                    'product_type_id' => 1,
                    'publish'         => 1,
                    'image_seo'       => null,
                    'keyword_seo'     => 'áo thun nam adidas có cổ đẹp, áo thun nam adidas có cổ giá shock, áo thun nam xách tay',
                    'description_seo' => 'đây là áo thun nam adidas có cổ thuộc shop adidas.vn là loại đồng hồ luôn uy tín chất lượng hàng đầu. liên hệ với chúng tôi để đực đặt hàng ttos',
                    'created_at'      => date('Y-m-d H:i:s'),
                    'updated_at'      => date('Y-m-d H:i:s')
                ],
                [
                    'name'            => 'áo thun nam an phước có cổ',
                    'slug'            => 'ao-thun-nam-an-phuoc-co-co',
                    'price'           => '2000',
                    'price_sale'      => '1300',
                    'price_unit'      => '¥',
                    'description'     => 'áo thun nam an phước có cổ,áo thun nam an phước có cổ xuất khẩu, áo thun nam ngoại, áo thun nam chất lượng, áo thun nam giá rẻ, đồng hồ xách tay',
                    'count'           => '-1',
                    'count_unit'      => 'cái',
                    'thumbnail'       => null,
                    'post_by_user_id' => 1,
                    'product_type_id' => 1,
                    'publish'         => 1,
                    'image_seo'       => null,
                    'keyword_seo'     => 'áo thun nam an phước có cổ đẹp, áo thun nam an phước có cổ giá shock, áo thun nam xách tay',
                    'description_seo' => 'đây là áo thun nam an phước có cổ thuộc shop an-phước.vn là loại đồng hồ luôn uy tín chất lượng hàng đầu. liên hệ với chúng tôi để đực đặt hàng ttos',
                    'created_at'      => date('Y-m-d H:i:s'),
                    'updated_at'      => date('Y-m-d H:i:s')
                ],
                [
                    'name'            => 'áo thun nam an phước có cổ',
                    'slug'            => 'ao-thun-nam-an-phuoc-co-co-2',
                    'price'           => '2000',
                    'price_sale'      => '1300',
                    'price_unit'      => '¥',
                    'description'     => 'user 2 áo thun nam an phước có cổ,áo thun nam an phước có cổ xuất khẩu, áo thun nam ngoại, áo thun nam chất lượng, áo thun nam giá rẻ, đồng hồ xách tay',
                    'count'           => '-1',
                    'count_unit'      => 'cái',
                    'thumbnail'       => null,
                    'post_by_user_id' => 2,
                    'product_type_id' => 1,
                    'publish'         => 1,
                    'image_seo'       => null,
                    'keyword_seo'     => 'user 2 áo thun nam an phước có cổ đẹp, áo thun nam an phước có cổ giá shock, áo thun nam xách tay',
                    'description_seo' => 'user 2 đây là áo thun nam an phước có cổ thuộc shop an-phước.vn là loại đồng hồ luôn uy tín chất lượng hàng đầu. liên hệ với chúng tôi để đực đặt hàng ttos',
                    'created_at'      => date('Y-m-d H:i:s'),
                    'updated_at'      => date('Y-m-d H:i:s')
                ],
                [
                    'name'            => 'áo thun nam an phước k cổ',
                    'slug'            => 'ao-thun-nam-an-phuoc-k-co-2',
                    'price'           => '2000',
                    'price_sale'      => '1300',
                    'price_unit'      => '¥',
                    'description'     => 'user 2 áo thun nam an phước k cổ,áo thun nam an phước có cổ xuất khẩu, áo thun nam ngoại, áo thun nam chất lượng, áo thun nam giá rẻ, đồng hồ xách tay',
                    'count'           => '-1',
                    'count_unit'      => 'cái',
                    'thumbnail'       => null,
                    'post_by_user_id' => 2,
                    'product_type_id' => 1,
                    'publish'         => 1,
                    'image_seo'       => null,
                    'keyword_seo'     => 'user 2 áo thun nam an phước k cổ đẹp, áo thun nam an phước có cổ giá shock, áo thun nam xách tay',
                    'description_seo' => 'user 2 đây là áo thun nam an phước có cổ thuộc shop an-phước.vn là loại đồng hồ luôn uy tín chất lượng hàng đầu. liên hệ với chúng tôi để đực đặt hàng ttos',
                    'created_at'      => date('Y-m-d H:i:s'),
                    'updated_at'      => date('Y-m-d H:i:s')
                ],
                [
                    'name'            => 'áo sơ mi nữ an phước k cổ',
                    'slug'            => 'ao-so-mi-nu-an-phuoc-k-co-2',
                    'price'           => '2000',
                    'price_sale'      => '1300',
                    'price_unit'      => '¥',
                    'description'     => 'user sơ mi nữ áo thun nam an phước k cổ,áo thun nam an phước có cổ xuất khẩu, áo thun nam ngoại, áo thun nam chất lượng, áo thun nam giá rẻ, đồng hồ xách tay',
                    'count'           => '-1',
                    'count_unit'      => 'cái',
                    'thumbnail'       => null,
                    'post_by_user_id' => 2,
                    'product_type_id' => 2,
                    'publish'         => 1,
                    'image_seo'       => null,
                    'keyword_seo'     => 'sơ mi nữ k cổ đẹp, áo thun nam an phước có cổ giá shock, áo thun nam xách tay',
                    'description_seo' => 'sơ mi nữ có cổ thuộc shop an-phước.vn là loại đồng hồ luôn uy tín chất lượng hàng đầu. liên hệ với chúng tôi để đực đặt hàng ttos',
                    'created_at'      => date('Y-m-d H:i:s'),
                    'updated_at'      => date('Y-m-d H:i:s')
                ],
                [
                    'name'            => 'áo sơ mi nữ adidas k cổ',
                    'slug'            => 'ao-so-mi-nu-adidas-k-co-2',
                    'price'           => '2000',
                    'price_sale'      => '1300',
                    'price_unit'      => '¥',
                    'description'     => 'user sơ mi nữ adidas k cổ,adidasước có cổ xuất khẩu, áo thun nam ngoại, áo thun nam chất lượng, áo thun nam giá rẻ, đồng hồ xách tay',
                    'count'           => '-1',
                    'count_unit'      => 'cái',
                    'thumbnail'       => null,
                    'post_by_user_id' => 2,
                    'product_type_id' => 2,
                    'publish'         => 1,
                    'image_seo'       => null,
                    'keyword_seo'     => 'sơ mi nữ k cổ đẹp, adidas có cổ giá shock, áo thun nam xách tay',
                    'description_seo' => 'sơ mi nữ có cổ thuộc shop an-phước.vn là loại đồng hồ luôn uy tín chất lượng hàng đầu. liên hệ với chúng tôi để đực đặt hàng ttos',
                    'created_at'      => date('Y-m-d H:i:s'),
                    'updated_at'      => date('Y-m-d H:i:s')
                ]
            ]
        );
    }
}
