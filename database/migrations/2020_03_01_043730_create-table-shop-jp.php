<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableShopJp extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        /**
         * rating for seo website
         */
        if (!Schema::hasTable('rating')) {
            Schema::create('rating', function (Blueprint $table) {
                $table->increments('id');
                $table->string('username', 40)->nullable();
                $table->string('avatar')->nullable();
                $table->timestamps();
            });
        }
        /**
         * table save category of website
         */
        if (!Schema::hasTable('product_category')) {
            Schema::create('product_category', function (Blueprint $table) {
                $table->increments('id');
                $table->string('name')->nullable();
                $table->string('slug')->nullable();
                $table->string('excerpt')->nullable();
                $table->string('thumbnail')->nullable();
                $table->string('background')->nullable();
                $table->string('description')->nullable();
                $table->string('image_seo')->nullable();
                $table->string('keyword_seo')->nullable(); /// từ khóa
                $table->string('description_seo')->nullable(); /// mô tả từ khóa
                $table->timestamps();
            });
        }
        /**
         * table save type product such as : category : quần áo -> type : áo thun 
         */
        if (!Schema::hasTable('product_type')) {
            Schema::create('product_type', function (Blueprint $table) {
                $table->increments('id');
                $table->string('name')->nullable();
                $table->string('slug')->nullable();
                $table->string('excerpt')->nullable();
                $table->string('thumbnail')->nullable();
                $table->string('background')->nullable();
                $table->string('description')->nullable();
                $table->unsignedInteger('product_category_id')->unsigned();
                $table->foreign('product_category_id')->references('id')->on('product_category')
                ->onDelete('CASCADE');
                $table->string('image_seo')->nullable();///save : web_image , og:image, ... NOT (alt title width height)
                $table->string('keyword_seo')->nullable(); /// từ khóa
                $table->string('description_seo')->nullable(); /// mô tả từ khóa
                $table->timestamps();
            });
        }
        /**
         * table save list product data
         */
        if (!Schema::hasTable('product')) {
            Schema::create('product', function (Blueprint $table) {
                $table->increments('id');
                $table->string('name')->nullable();
                $table->string('slug')->nullable();  /// slug
                $table->string('price')->nullable(); /// giá ban đầu
                $table->string('price_sale')->nullable();
                $table->string('price_unit')->nullable();
                $table->string('description')->default(0); ///% giảm giá
                $table->string('count')->default(0); /// tồn kho
                $table->string('count_unit')->nullable(); ///đơn vị số lượng
                $table->string('thumbnail')->nullable();
                $table->unsignedInteger('post_by_user_id')->unsigned();
                $table->unsignedInteger('product_type_id')->unsigned();
                $table->foreign('product_type_id')->references('id')
                ->on('product_type')->onDelete('CASCADE');
                $table->integer('like')->default(1);
                $table->integer('view')->default(1);
                $table->integer('publish')->default(1);
                $table->text('image_seo')->nullable();///save : web_image , og:image, ...
                $table->text('keyword_seo')->nullable(); /// từ khóa
                $table->text('description_seo')->nullable(); /// mô tả từ khóa
                $table->timestamps();
            });
        }


        /**
         * rating for activity of product
         */
        if (!Schema::hasTable('rating_active')) {
            Schema::create('rating_active', function (Blueprint $table) {
                $table->increments('id');
                $table->unsignedInteger('product_id')->unsigned();
                $table->foreign('product_id')->references('id')->on('product')
                ->onDelete('CASCADE');
                $table->unsignedInteger('rating_id')->unsigned();
                $table->foreign('rating_id')->references('id')->on('rating')
                ->onDelete('CASCADE');
                $table->timestamps();
            });
        }
        /**
         * product style for product data have multi data search
         */
        if (!Schema::hasTable('product_style')) {
            Schema::create('product_style', function (Blueprint $table) {
                $table->increments('id');
                $table->string('name')->nullable();
                $table->string('slug')->nullable();
                $table->string('excerpt')->nullable();
                $table->string('thumbnail')->nullable();
                $table->string('background')->nullable();
                $table->string('description')->nullable();
                $table->string('image_seo')->nullable();
                $table->string('description_seo')->nullable();
                $table->string('keyword_seo')->nullable();
                $table->unsignedInteger('product_type_id')->unsigned();
                $table->timestamps();
            });
        }
        /**
         * product activity of product style
         */
        if (!Schema::hasTable('product_active_style')) {
            Schema::create('product_active_style', function (Blueprint $table) {
                $table->increments('id');
                $table->bigInteger('product_id')->nullable();
                $table->bigInteger('style_id')->nullable();
                $table->timestamps();
            });
        }
        /**
         * save image to db then render data seo website
         */
        if (!Schema::hasTable('picture')) {
            Schema::create('picture', function (Blueprint $table) {
                $table->increments('id');
                $table->string('src');
                $table->string('alt')->nullable();
                $table->string('width', 10)->nullable();
                $table->string('height', 10)->nullable();
                $table->string('title')->nullable();
                $table->string('galery')->nullable();
                $table->bigInteger('foreign_id');
                $table->string('table', 20);
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('picture');
        Schema::dropIfExists('product_active_style');
        Schema::dropIfExists('product_style');
        Schema::dropIfExists('product');
        Schema::dropIfExists('product_type');
        Schema::dropIfExists('product_category');
        Schema::dropIfExists('rating');
        Schema::dropIfExists('rating_active');
    }
}
