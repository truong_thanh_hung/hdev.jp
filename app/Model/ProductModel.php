<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ProductModel extends Model
{
    protected $table = 'product';
    /**
     * là mối quan hệ dạng nhiều nhiều ví dụ : product -> activity -> style thì thứ tự sẽ là như dưới
     */
    public function product_style(){
        return $this->belongsToMany(ProductStyleModel::class, 'product_active_style', 'product_id', 'style_id')->get();
    }
}
