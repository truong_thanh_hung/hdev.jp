<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ProductStyleModel extends Model
{
    protected $table = 'product_style';
}
