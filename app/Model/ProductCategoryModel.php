<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ProductCategoryModel extends Model
{
    protected $table = 'product_category';
    /*
     * lấy dữ liệu: from relation 1 -> n then use hasMany
     * */
    public function product_type(){
        return $this->hasMany(ProductTypeModel::class, 'product_category_id', 'id')->get();
    }
}
