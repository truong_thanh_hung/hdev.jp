<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ProductTypeModel extends Model
{
    protected $table = 'product_type';
    /**
     * from n ->  1 : use belongsTo
     */
    public function product_category(){
        return $this->belongsTo(ProductCategoryModel::class, 'product_category_id', 'id')->first();
    }
    /**
     * from 1 -> n : use hasMany
     */
    public function product(){
        return $this->hasMany(ProductModel::class, 'product_type_id', 'id')->get();
    }
}
