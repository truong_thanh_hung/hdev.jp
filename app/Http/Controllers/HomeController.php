<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index(){
        return view('Front.index');
    }
    public function list_product($slug){
        switch ($slug) {
            case 'dong-ho':
                return $this->list_dongho();
            break;
            default:
                return redirect()->route('404');
        }
    }
    public function cart(){
        return view('Front.cart');
    }
    public function about(){
        return view('Front.about');
    }
    public function contact(){
        return view('Front.contact');
    }
    public function blog(){
        return view('Front.blog');
    }
    public function load_product($slug){
        switch($slug) {
            case 'dong-ho' :{
                $data = [
                    [
                        'id' => 93,
                        'name' => 'Herschel supply co 25l ajax',
                        'price' => 75.00,
                        'typeMoney' => '$',
                        'typeShow' => 'block2-labelsale', //block2-labelnew
                        'like' => 0,
                        'addCart' => 0,
                        'image' => [
                            'src' => asset('teamplate/images/item-01.jpg'),
                            'alt' => '',
                            'title' => ''
                        ]
                    ],
                    [
                        'id' => 92,
                        'name' => 'Herschel supply co 25l ajax',
                        'price' => 75.00,
                        'typeMoney' => '$',
                        'typeShow' => 'block2-labelsale', //block2-labelnew
                        'like' => 0,
                        'addCart' => 0,
                        'image' => [
                            'src' => asset('teamplate/images/item-02.jpg'),
                            'alt' => '',
                            'title' => ''
                        ]
                    ],
                    [
                        'id' => 91,
                        'name' => 'Herschel supply co 25l ajax',
                        'price' => 75.00,
                        'typeMoney' => '$',
                        'typeShow' => 'block2-labelsale', //block2-labelnew
                        'like' => 0,
                        'addCart' => 0,
                        'image' => [
                            'src' => asset('teamplate/images/item-03.jpg'),
                            'alt' => '',
                            'title' => ''
                        ]
                    ],
                    [
                        'id' => 96,
                        'name' => 'Herschel supply co 25l ajax',
                        'price' => 75.00,
                        'typeMoney' => '$',
                        'typeShow' => 'block2-labelsale', //block2-labelnew
                        'like' => 1,
                        'addCart' => 0,
                        'image' => [
                            'src' => asset('teamplate/images/item-04.jpg'),
                            'alt' => '',
                            'title' => ''
                        ]
                    ],
                    [
                        'id' => 89,
                        'name' => 'Herschel supply co 25l ajax',
                        'price' => 75.00,
                        'typeMoney' => '$',
                        'typeShow' => 'block2-labelsale', //block2-labelnew
                        'like' => 0,
                        'addCart' => 0,
                        'image' => [
                            'src' => asset('teamplate/images/item-05.jpg'),
                            'alt' => '',
                            'title' => ''
                        ]
                    ],
                    [
                        'id' => 67,
                        'name' => 'Herschel supply co 25l ajax',
                        'price' => 75.00,
                        'typeMoney' => '$',
                        'typeShow' => 'block2-labelsale', //block2-labelnew
                        'like' => 0,
                        'addCart' => 0,
                        'image' => [
                            'src' => asset('teamplate/images/item-06.jpg'),
                            'alt' => '',
                            'title' => ''
                        ]
                    ],
                    [
                        'id' => 56,
                        'name' => 'Herschel supply co 25l ajax',
                        'price' => 75.00,
                        'typeMoney' => '$',
                        'typeShow' => 'block2-labelsale', //block2-labelnew
                        'like' => 0,
                        'addCart' => 0,
                        'image' => [
                            'src' => asset('teamplate/images/item-07.jpg'),
                            'alt' => '',
                            'title' => ''
                        ]
                    ],
                    [
                        'id' => 34,
                        'name' => 'Herschel supply co 25l ajax',
                        'price' => 73.00,
                        'typeMoney' => '$',
                        'typeShow' => 'block2-labelsale', //block2-labelnew
                        'like' => 0,
                        'addCart' => 0,
                        'image' => [
                            'src' => asset('teamplate/images/item-08.jpg'),
                            'alt' => '',
                            'title' => ''
                        ]
                    ]
                ];
                $response = [
                    'status' => true,
                    'data' => $data
                ];
                return response()->json($response, 200);
                break;
            }
        }
    }
    public function product_detail($id){
        return view('Front.product_detail');
    }
    private function list_dongho(){
        return view('Front.product');
    }
}
