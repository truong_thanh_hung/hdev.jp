<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Model\Md_ProductCategory;
use App\Model\Md_ProductType;
use App\Model\Md_ProductStyle;
use App\Model\Md_Rating;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\View;
use Config;
use Session;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);
        if(!app()->runningInConsole())
        {
            $dataPublic = [
                'APP_URL' => config('app.url'),
                'APP_NAME' => config('app.name'),
                'APP_LOCATE' => \Session::get('website_language', config('app.locale'))
            ];
            View::share('ShareDataPublic' , $dataPublic);
            if(!app()->runningInConsole())
            {
                $industry = Md_ProductCategory::Select('industry')->GroupBy('industry')->get();
                View::share('Data_All_View_Model_Name' , $industry);
                $rating = new Md_Rating();
                View::share('People_Rating' , $rating->all());
                View::share('data_product_type' , Md_ProductType::OrderBy('product_category_id' , 'ASC')->get());
                View::share('data_product_category' , Md_ProductCategory::ALl());
                View::share('data_product_style' , Md_ProductStyle::ALl());
            }
        }
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
