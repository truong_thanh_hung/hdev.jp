<?php

namespace App\Providers;

use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use App\Model\Md_Permission;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        Gate::define('is_supper_admin', function ($user) {
            return ($user->role)->type == 'admin' && strpos(".".$user->email , 'admin@herowebe.com');
        });
        Gate::define('is_admin', function ($user) {
            return ($user->role)->type == 'admin' ;
        });
        Gate::define('is_user', function ($user) {
            return ($user->role)->type == 'user' ;
        });
        if(!$this->app->runningInConsole()){
            foreach (Md_Permission::all() as $permission ){
                Gate::define($permission->name, function ($user) use ($permission) {
                    
                    return $user->check_Permission($permission->name);
                });
            }
        }
    }
}
